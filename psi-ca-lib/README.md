# Decentralized PSI Cardinality library 

- to install bazel, run the setup_bzl.sh script with sudo previleges. 
- before running the build.sh script, install the required dependencies for the python code via pip (pip install -r ./python/requirements.txt).
- after running build.sh, the generated python package can be found in the fpsica directory. 